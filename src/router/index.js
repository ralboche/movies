import Vue from 'vue'
import Router from 'vue-router'
import Vuetify from 'vuetify'

import HelloWorld from '@/views/Home'
import Movies from '@/views/movies/Index'
import CreateMovie from '@/views/movies/Create'

Vue.use(Router)
Vue.use(Vuetify)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HelloWorld
    },
    {
      path: '/movies',
      name: 'Movies',
      component: Movies
    },
    {
      path: '/movies/create',
      name: 'CreateMovie',
      component: CreateMovie
    }
  ]
})
