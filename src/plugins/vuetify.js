import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify)

const opts = {
  icons: {
    iconfont: 'mdiSvg'
  },
  theme: {
    themes: {
      light: {
        primary: colors.blue.darken4, // #E53935
        secondary: colors.cyan.lighten5, // #FFCDD2
        accent: colors.cyan.lighten2 // #3F51B5
      }
    }

  }

}

export default new Vuetify(opts)
