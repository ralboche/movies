import Vue from 'vue'
import App from './App'
import router from './router'
import vuetify from './plugins/vuetify'

import Header from './components/Header.vue'
import Footer from './components/Footer.vue'

Vue.component('Header', Header)
Vue.component('Footer', Footer)

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
